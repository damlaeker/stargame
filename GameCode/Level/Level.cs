﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamlaEker
{
    class Level
    {
        public string sDescription;
        public int iNoEggs;
        public int iTotalTime;
        public int iNoMoon;
        public string sBackground;
        public List<Evil> TheEvils;
        public List<Tile> TheTiles;
        public List<Egg> TheEggs;
        public List<Moons> TheMoons;
    }

    class Evil
    {
        public int iEvilType = 0;
	    public List<Coordinate> Coordinates; 
    }

    class Tile
    { 
        public int iTileType =0;
        public int PosX = 0;
        public int PosY = 0;
    }
    class Egg
    {
        public int iEggType = 0;
        public int PosX = 0;
        public int PosY = 0;
    }
    class Moons
    {
        public int iMoonType = 0;
        public int PosX = 0;
        public int PosY = 0;
    }

    class Coordinate
    {
        public int X;
        public int Y;
    }

}
