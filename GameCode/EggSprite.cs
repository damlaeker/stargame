﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using HologramSpriteManager;

namespace DamlaEker
{
    class EggSprite : AnimatedSprite
    {
        public EggSprite(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
        }
        public Status CurrentStatus = Status.Idle;
        public enum Status
        {
            Idle,
            Carried,
            Basket
        }

        public Player.Direction MyDirection = Player.Direction.down;

        public EggSprite()
        {

        }

        public void Update()
        {

            if (CurrentStatus == Status.Idle)
            {
                if (GameStatic.ThePlayer.Bounds.Intersects(Bounds))
                {
                    TakeMe();
                    GameStatic.ThePlayer.bIsCarrying = true;
                   
                }

            }
            if (CurrentStatus == Status.Carried)
            {
                Position.X = GameStatic.ThePlayer.Position.X + 10;
                Position.Y = GameStatic.ThePlayer.Position.Y - 10;
            }

            
        }
    
        

        public void TakeMe()
        {
            CurrentStatus = Status.Carried;
        }

        public void DropMe()
        {
            if (CurrentStatus == Status.Carried)
            {
                CurrentStatus = Status.Idle;
                Position = GameStatic.ThePlayer.Position;
            }
        }

    }
}

