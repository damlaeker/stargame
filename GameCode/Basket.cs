﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using HologramSpriteManager;

namespace DamlaEker
{
    class Basket : AnimatedSprite
    {
         public Basket(AnimatedSpriteSequences Meta)
      : base(Meta){
    Position = new Vector2(850,550);
    
}
         public void Update()
         {
             if (Bounds.Intersects(GameStatic.ThePlayer.Bounds) && GameStatic.ThePlayer.bIsCarrying) 
             {
                 GameStatic.PutEggsInBasket();


             }
         }
      
    }

 
}
