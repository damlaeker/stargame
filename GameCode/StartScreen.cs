﻿#region
using System;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using HologramSpriteManager;

using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using DamlaEker;
#endregion

namespace DamlaEker
{
    class StartScreen
    {
        public Texture2D BG;
        
        public static FontRenderer MainFont;
       
        

        public void Draw()
        {
            SpriteManager.spriteBatch.Draw(BG, new Rectangle(0, 0, 1000, 700), Color.White);
            MainFont.DrawTextWithWidth(new Vector2(250, 350), 1000, "Press Space to Start)", 1, Color.Turquoise);
        }
        public void Update()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                GameStatic.SetLevel(0);
                Game1.CurrentScreen = Game1.Screens.Game;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                GameStatic.SetLevel(0);
                Game1.CurrentScreen = Game1.Screens.Game;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D2))
            {
                GameStatic.SetLevel(1);
                Game1.CurrentScreen = Game1.Screens.Game;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.C))
            {
                GameStatic.SetLevel(2);
                Game1.CurrentScreen = Game1.Screens.Game;
            }
            

        }
    }
}