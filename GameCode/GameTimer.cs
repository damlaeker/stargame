﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace DamlaEker
{
    class GameTimer:GameComponent
    {
        private SpriteFont font;
        private String text;
        private float time;
        private Vector2 position;
        private bool started;
        private bool paused;
        private bool finished;


        public GameTimer(Game game,float startTime)
            : base(game)
        {
            time = startTime;
            started = false;
            paused = false;
            finished = false;
            Text = "";
        }


        public void SetStartTime(float fNewStartTime)
        {
            time = fNewStartTime;
        }

        public SpriteFont Font
        {
            get { return font; }
            set { font = value; }
        }
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public bool Started 
        {
            get { return started; }
            set { started = value; }
        }

        public bool Paused
        {
            get { return paused; }
            set { paused = value; }
        }
        public bool Finished
        {
            get { return finished; }
            set { started = value; }
        }
    public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

    public override void Update(GameTime gameTime)
    {
        float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
        if (started)
        {
            if (!paused)
            {
                if (time > 0)

                    time -= deltaTime;
                else
                    finished = true;
                                
            }


            Text = time.ToString();

            base.Update(gameTime);
        }
    }
 
    public  void Draw(SpriteBatch SpriteBatch)
    {
        StartScreen.MainFont.DrawTextWithWidth(new Vector2(600, 5), 1000, "Time " + (int)time, 1, Color.Black);
    }



    
    }
}
