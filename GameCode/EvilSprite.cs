﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using HologramSpriteManager;
using Microsoft.Xna.Framework.Graphics;

namespace DamlaEker
{
    class EvilSprite : AnimatedSprite
    {
        public EvilSprite(AnimatedSpriteSequences Meta)
            : base(Meta) {
                iCoordinatePos = 0;
                
            }
        public EvilSprite()
        {
            
        }

        public List<Coordinate> Coordinates; 

        Direction CurrentDirection = Direction.Right;
        // Direction2 CurrentDirection2 = Direction2.Up;

        enum Direction
         {
             Left,
             Right,
             Up,
             Down

         }


         int speed=2;


         int iCoordinatePos = 0;
         float fStartMoveTime;
         Vector2 StartPosition;
         Vector2 EndPosition;

         void SetCoordinateToNextNode()
         {
             iCoordinatePos++;
             if (iCoordinatePos > Coordinates.Count - 1)
                 iCoordinatePos = 0;


             SetCoordinateToNode();
         }

         public void SetCoordinateToNode( )
         {
             StartPosition.X = Coordinates[iCoordinatePos].X;
             StartPosition.Y = Coordinates[iCoordinatePos].Y;
             Position = StartPosition;
             int iNextPos=iCoordinatePos+1;
             if (iNextPos > Coordinates.Count - 1)
                 iNextPos = 0;

             EndPosition.X = Coordinates[iNextPos].X;
             EndPosition.Y = Coordinates[iNextPos].Y;

             fStartMoveTime = SpriteManager.GameTime;

         }

         void EvaluatePosition()
         {
             float Distance = Vector2.Distance(StartPosition, EndPosition);
             float fTimePassed = SpriteManager.GameTime - fStartMoveTime;

             float TimeItShouldTake = (Distance / speed) *20;

             if(fTimePassed>=TimeItShouldTake)
             {
                 //Move to next node!
                SetCoordinateToNextNode();
                return;
             }

             Console.WriteLine(TimeItShouldTake + " : " + fTimePassed);
             float fPassedAmount = (fTimePassed) / TimeItShouldTake;

             Vector2 NewPosition = Vector2.Lerp(StartPosition, EndPosition, fPassedAmount);
             Position = NewPosition;
         }

         void SetAnimation()
         {
             //set direction
             //if the X changes it uses the sideways animation set X to equal on both nodes to use the up / down animations

             if (StartPosition.X > EndPosition.X)
                 CurrentDirection = Direction.Left;
             else if (StartPosition.X < EndPosition.X)
                 CurrentDirection = Direction.Right;
             else if (StartPosition.Y < EndPosition.Y)
                 CurrentDirection = Direction.Down;
             else if (StartPosition.Y >EndPosition.Y)
                 CurrentDirection = Direction.Up;


             //set animation
             if (CurrentDirection == Direction.Right)
             {
                 ChangeAnimation("walkright", false);
             }

             if (CurrentDirection == Direction.Left)
             {
                 ChangeAnimation("walkleft", false);
             }

             if (CurrentDirection == Direction.Up)
             {
                 ChangeAnimation("walkup", false);
             }

             if (CurrentDirection == Direction.Down)
             {
                 ChangeAnimation("walkdown", false);
             }

         }

        public void Update()
         {
             EvaluatePosition();

             SetAnimation();
             

             //collison
             if (GameStatic.ThePlayer.Bounds.Intersects(Bounds))
             {
                 GameStatic.PlayerHit();
             }
             
         }
    }
}
