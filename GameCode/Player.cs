﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using HologramSpriteManager;


namespace DamlaEker
{



    class Player : AnimatedSprite
    {
        public Player(AnimatedSpriteSequences Meta)
            : base(Meta) {
                Position = new Vector2(800, 600);
            }


        public Direction CurrentDirection = Direction.down;
        public enum Direction
        {
            left,
            right,
            up,
            down
        }

        public State CurrentState = State.Normal;
        public enum State
        { 
            Normal,
            Happy
        }


        public bool bIsCarrying = false;
        public int iSpeed = 2;

        public void Reset()
        {
            Position = new Vector2(800, 600);
            bIsCarrying = false;
        }



        public void Update()
        {
            if (CurrentState == State.Normal)
                Move();

            if (CurrentState == State.Happy)
            {
                float fTimePassed = SpriteManager.GameTime - fHappyStart;
                if (fTimePassed >= fHappyDuration)
                {
                    CurrentState = State.Normal;
                }
            }
           
        }


        public void Move()
        {

            bool bCanMoveLeft = true;
            bool bCanMoveRight = true;
            bool bCanMoveUp = true;
            bool bCanMoveDown = true;

            GameStatic.CheckMovement(out bCanMoveLeft, out  bCanMoveRight, out bCanMoveUp, out bCanMoveDown);
            
            if (bIsCarrying)
            {
                iSpeed = 2;
               
            }
            else
            {
                iSpeed = 4;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                if (Position.X > 10 && bCanMoveLeft)
                {
                    Position.X -= iSpeed;
                    ChangeAnimation("walkleft", false);
                    CurrentDirection = Direction.left;
                }

            }

            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                if (Position.X < 980 && bCanMoveRight)
                {
                    Position.X += iSpeed;

                    ChangeAnimation("walkright", false);
                    CurrentDirection = Direction.right;
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                if (Position.Y > 10 && bCanMoveUp)
                {
                    Position.Y -= iSpeed;
                    ChangeAnimation("walkup", false);
                    CurrentDirection = Direction.up;
                   
                }

            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                if (Position.Y < 650 && bCanMoveDown)
                {
                    Position.Y += iSpeed;
                    ChangeAnimation("walkdown", false);
                    CurrentDirection = Direction.down;

                }

            }
            else
            {
                switch (CurrentDirection)
                {
                    case Direction.down:
                        ChangeAnimation("idle");
                        break;
                    case Direction.left:
                        ChangeAnimation("idleleft");
                        break;
                    case Direction.right:
                        ChangeAnimation("idleright");
                        break;
                    case Direction.up:
                        ChangeAnimation("idleup");
                        break;

                }
                
            }

        }
        

        float fHappyDuration=600;
        float fHappyStart=0;
        public void AnimateHappy()
        {
            fHappyStart = SpriteManager.GameTime;
            CurrentState = State.Happy;
            ChangeAnimation("happy");
        }
    }

}
