﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DamlaEker
{
    class GameStatic
    {

        public static GameTimer timer;
      
        
        //the sprites
        public static List<EvilSprite> TheEvils;
        public static List<EggSprite> TheEggs;
        public static List<TileSprite> TheTiles;
        public static List<Moon> TheMoons;

        //the levels
        public static List<Level> Levels;
        public static int iLevel;
        public static int iLevelTime;
        public static int iEggsToWin;

        
        //the current objects
        public static List<EvilSprite> CurrentEvils;
        public static List<EggSprite> CurrentEggs;
        public static List<TileSprite> CurrentTiles;
        public static List<Moon> CurrentMoons;
        public static Player ThePlayer;
        public static Basket TheBasket;
        public static Texture2D Background;
        //public static Texture2D Background1;
       
        public static void CheckMovement(out bool bCanMoveLeft, out bool bCanMoveRight, out bool bCanMoveUp, out bool bCanMoveDown)
        {
            bCanMoveLeft = true;
            bCanMoveRight = true;
            bCanMoveUp = true;
            bCanMoveDown = true;
            for (int i = 0; i < CurrentTiles.Count; i++)
            {
                TileSprite CurrentTile = CurrentTiles[i];
                if (CurrentTile.CheckIntersect(ThePlayer))
                { 
                    List<Collision> Collisions = CurrentTile.CheckAllCollisions(ThePlayer);
                    if(Collisions.Count>0)
                    {
                        for (int j = 0; j < Collisions.Count; j++)
                        {
                            if (Collisions[j].bCollided)
                            {
                                if (Collisions[j].LocalGroup == "TOP")
                                {
                                    bCanMoveDown = false;
                                }
                                if (Collisions[j].LocalGroup == "BOTTOM")
                                {
                                    bCanMoveUp = false;
                                }
                                if (Collisions[j].LocalGroup == "LEFT")
                                {
                                    bCanMoveRight = false;
                                }
                                if (Collisions[j].LocalGroup == "RIGHT")
                                {
                                    bCanMoveLeft = false;
                                }
                            }
                        }                        
                    }
                }
            }
           
                
                if (TheBasket.CheckIntersect(ThePlayer))
                {
                    List<Collision> Collisions = TheBasket.CheckAllCollisions(ThePlayer);
                    if (Collisions.Count > 0)
                    {
                        for (int j = 0; j < Collisions.Count; j++)
                        {
                            if (Collisions[j].bCollided)
                            {
                                if (Collisions[j].LocalGroup == "TOP")
                                {
                                    bCanMoveDown = false;
                                }
                                if (Collisions[j].LocalGroup == "BOTTOM")
                                {
                                    bCanMoveUp = false;
                                }
                                if (Collisions[j].LocalGroup == "LEFT")
                                {
                                    bCanMoveRight = false;
                                }
                                if (Collisions[j].LocalGroup == "RIGHT")
                                {
                                    bCanMoveLeft = false;
                                }
                            }
                        }
                    }
                }
            

        }

        public static void TakeEggs(out bool bLeft, out bool bRight, out bool bUp, out bool bDown)
        {
            bLeft = false;
            bRight = false;
            bUp = false;
            bDown = false;
            for (int i = 0; i < CurrentEggs.Count; i++)
            {
                EggSprite CurrentEgg = CurrentEggs[i];
                if (CurrentEgg.CheckIntersect(ThePlayer))
                {
                    List<Collision> Collisions = CurrentEgg.CheckAllCollisions(ThePlayer);
                    if (Collisions.Count > 0)
                    {
                        for (int j = 0; j < Collisions.Count; j++)
                        {
                            if (Collisions[j].bCollided)
                            {
                                if (Collisions[j].LocalGroup == "TOP")
                                {
                                    bDown = true;
                                }
                                if (Collisions[j].LocalGroup == "BOTTOM")
                                {
                                    bUp = true;
                                }
                                if (Collisions[j].LocalGroup == "LEFT")
                                {
                                    bRight = true;
                                }
                                if (Collisions[j].LocalGroup == "RIGHT")
                                {
                                    bLeft = true;
                                }
                            }
                        }
                    }
                }
            }
        }

            public static void TakeMoon(out bool bLeft, out bool bRight, out bool bUp, out bool bDown)
        {
            bLeft = false;
            bRight = false;
            bUp = false;
            bDown = false;
            for (int i = 0; i < CurrentMoons.Count; i++)
            {
                Moon CurrentMoon = CurrentMoons[i];
                if (CurrentMoon.CheckIntersect(ThePlayer))
                {
                    List<Collision> Collisions = CurrentMoon.CheckAllCollisions(ThePlayer);
                    if (Collisions.Count > 0)
                    {
                        for (int j = 0; j < Collisions.Count; j++)
                        {
                            if (Collisions[j].bCollided)
                            {
                                if (Collisions[j].LocalGroup == "TOP")
                                {
                                    bDown = true;
                                }
                                if (Collisions[j].LocalGroup == "BOTTOM")
                                {
                                    bUp = true;
                                }
                                if (Collisions[j].LocalGroup == "LEFT")
                                {
                                    bRight = true;
                                }
                                if (Collisions[j].LocalGroup == "RIGHT")
                                {
                                    bLeft = true;
                                }
                            }
                        }
                    }
                }
            }


        }

        

        public static void LoadLevels(int iCount)
        
          {
            Levels = new List<Level>();
            for (int i = 0; i < iCount; i++)
            {

                string text = System.IO.File.ReadAllText(@".\Content\Levels\Level" + (i+1) + ".json");
                Level CurrentLevel = JsonConvert.DeserializeObject<Level>(text);
                Levels.Add(CurrentLevel);

            }
        }

        public static void LoadTiles(int iCount)
        {
            TheTiles = new List<TileSprite>();
            
            for (int i = 0; i < iCount; i++)
            {
                string text = @".\Content\Specs\tileSpecs\tile" + (i + 1) + ".json";
                TileSprite CurrentTile = AnimationSpecReader.PopulateAnimations<TileSprite>(text);
                TheTiles.Add(CurrentTile);
            }
        }

        public static void LoadEggs(int iCount)
        {
            TheEggs = new List<EggSprite>();

            for (int i = 0; i < iCount; i++)
            {
                string text = @".\Content\Specs\egg1.json";
                EggSprite CurrentEgg = AnimationSpecReader.PopulateAnimations<EggSprite>(text);
                TheEggs.Add(CurrentEgg);
            }

         }

        public static void LoadMoon(int iCount)
        {
            TheMoons = new List<Moon>();

            for (int i = 0; i < iCount; i++)
            {
                string text = @".\Content\Specs\moon"+(i+1)+".json";
                Moon CurrentMoon = AnimationSpecReader.PopulateAnimations<Moon>(text);
                TheMoons.Add(CurrentMoon);
            }
        }

        public static void LoadEvils(int iCount)
        {
            TheEvils = new List<EvilSprite>();

            for (int i = 0; i < iCount; i++)
            {
                string text = @".\Content\Specs\evil" + (i + 1) + ".json";
                EvilSprite CurrentEvil = AnimationSpecReader.PopulateAnimations<EvilSprite>(text);
                TheEvils.Add(CurrentEvil);
            }


        }

        public static void bWin() 
        
        {
            timer.Paused = true;
            ThePlayer.ChangeAnimation("happy");
        }
        public static void SetLevel(int _iLevel)
        {

            iLevel = _iLevel;

            Level CurrentLevel = Levels[iLevel];
            
            timer.Started = false;
            timer.SetStartTime(CurrentLevel.iTotalTime);

            iLevelTime = CurrentLevel.iTotalTime;
            
            iEggsToWin = CurrentLevel.iNoEggs;
            CurrentEvils = new List<EvilSprite>();
            CurrentEggs = new List<EggSprite>();
            CurrentTiles = new List<TileSprite>();
            CurrentMoons = new List<Moon>();

            Background = SpriteManager.ContentShell.Load<Texture2D>("Sprites/bg(2)");
            //Background1 = SpriteManager.ContentShell.Load<Texture2D>("Sprites/tile");

            //current evils
            for (int i = 0; i < CurrentLevel.TheEvils.Count; i++)
            {
                EvilSprite current = new EvilSprite();
                current.Sequences  = TheEvils[CurrentLevel.TheEvils[i].iEvilType].Sequences.Clone();
                current.Coordinates = CurrentLevel.TheEvils[i].Coordinates;
                current.SetCoordinateToNode();
             
                //current.Position = new Vector2(CurrentLevel.TheEvils[i].Coordinates[0].X, CurrentLevel.TheEvils[i].Coordinates[0].Y);
                
                CurrentEvils.Add(current);
            }

            //current tiles
            for (int i = 0; i < CurrentLevel.TheTiles.Count; i++)
            {
                TileSprite current = new TileSprite();
                current.Sequences = TheTiles[CurrentLevel.TheTiles[i].iTileType].Sequences.Clone();
                current.Position = new Vector2(CurrentLevel.TheTiles[i].PosX, CurrentLevel.TheTiles[i].PosY);
                CurrentTiles.Add(current);
            }

            //current eggs
            for (int i = 0; i < CurrentLevel.TheEggs.Count; i++)
            {
                EggSprite current = new EggSprite();
                current.Sequences = TheEggs[CurrentLevel.TheEggs[i].iEggType].Sequences.Clone();
                current.Position = new Vector2(CurrentLevel.TheEggs[i].PosX, CurrentLevel.TheEggs[i].PosY);
                CurrentEggs.Add(current);
            }

            //for (int i = 0; i <= CurrentLevel.TheMoons.Count; i++)
            //{
            //    Moon current = new Moon();
            //    current.Sequences = TheMoons[CurrentLevel.TheMoons[i].iMoonType].Sequences.Clone();
            //    current.Position = new Vector2(CurrentLevel.TheMoons[i].PosX, CurrentLevel.TheMoons[i].PosY);
            //    CurrentMoons.Add(current);
            //}
            timer.Started = true;
        }
        
        public static void PutEggsInBasket()
        {
            int iEggCount=0;
            ThePlayer.bIsCarrying=false;
            for (int i = 0; i < CurrentEggs.Count; i++)
            {
                
                if(CurrentEggs[i].CurrentStatus == EggSprite.Status.Carried)
                {
                    iEggCount=0;
                   
                    for (int j = 0; j < CurrentEggs.Count; j++)
                    {
                        if (CurrentEggs[j].CurrentStatus == EggSprite.Status.Basket)
                            iEggCount++;
                    }
                    CurrentEggs[i].CurrentStatus = EggSprite.Status.Basket;
                    CurrentEggs[i].Position = new Vector2(880 + (10*iEggCount),610);
                    iEggCount++;
                    ThePlayer.AnimateHappy();
                    
                }
            }
         
            if (iEggCount >= iEggsToWin)
            {
                
                LevelSuccess();
                //TheTime.ResetTimer();
                                
            }

        }
       
        public static void LevelSuccess()
        {

            WaitForLevelChange();
      
           
        }
        
        static float fLevelChangePeriod = 700;
        static float fWaitStartTime; 
        static bool bWaitingForLevelChange = false; 
        
        static void WaitForLevelChange()
        {
            fWaitStartTime = SpriteManager.GameTime;
            bWaitingForLevelChange = true;
        }
       

        public static void Update()
        {

            if (bWaitingForLevelChange)
            {
                float fTimePassed = SpriteManager.GameTime - fWaitStartTime;
                if (fTimePassed >= fLevelChangePeriod)
                {
                    bWaitingForLevelChange = false;
                    SetLevel(iLevel + 1);
                }
            }

            for (int i = 0; i < CurrentEvils.Count; i++)
            {
                CurrentEvils[i].Update();
            }
            for (int i = 0; i < CurrentEggs.Count; i++)
            {
                CurrentEggs[i].Update();
            }
            for (int i = 0; i < CurrentTiles.Count; i++)
            {
                CurrentTiles[i].Update();
            }
            for (int i = 0; i < CurrentMoons.Count; i++)
            {
                CurrentMoons[i].Update();
            }
           


            ThePlayer.Update();
            TheBasket.Update();
            
            //check player to egg collision
           

            //check egg to basket collision

        }

        public static void PlayerHit()
        {
            for(int i=0;i<CurrentEggs.Count;i++)
            {
                CurrentEggs[i].DropMe();
            }
            ThePlayer.Reset();
        }

        public static void Draw()
        {
            SpriteManager.spriteBatch.Draw(Background, new Rectangle(0, 0, 1000, 700), Color.White);
            //SpriteManager.spriteBatch.Draw(Background1, new Rectangle(0, 0, 1000, 700), Color.White);
            TheBasket.Draw();

            if (!bWaitingForLevelChange)
            {
                for (int i = 0; i < CurrentTiles.Count; i++)
                {
                    CurrentTiles[i].Draw();
                }
                for (int i = 0; i < CurrentEggs.Count; i++)
                {
                    CurrentEggs[i].Draw();
                }
                for (int i = 0; i < CurrentEvils.Count; i++)
                {
                    CurrentEvils[i].Draw();
                }
                for (int i = 0; i < CurrentMoons.Count; i++)
                {
                    CurrentMoons[i].Draw();
                }
            }                     
                        
            ThePlayer.Draw();
            //if ()
            //{
                
            //    StartScreen.MainFont.DrawTextWithWidth(new Vector2(350, 400), 1000, "GAME OVER)", 1, Color.Black);
            //}
            

        }




       
    }
}
