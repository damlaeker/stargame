﻿#region
using System;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using HologramSpriteManager;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;



#endregion


namespace DamlaEker
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        
        Song bgMusic;




       

        StartScreen TheStartScreen;
        GameScreen TheGameScreen;
        public static Screens CurrentScreen = Screens.Start;


        public enum Screens
        {
            Start,
            Pause,
            Game
        }
       
        

        public Game1()
        {
             
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = 700;
            graphics.PreferredBackBufferWidth = 1000;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            

            SpriteManager.GameTime = 0;
          
            base.Initialize();
        }
      
        Texture2D gameover;
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        /// 

       
        protected override void LoadContent()
        {

            SpriteManager.Initialise(new SpriteBatch(GraphicsDevice), Content, graphics);

            GameStatic.LoadTiles(10);
            GameStatic.LoadEggs(3);
            GameStatic.LoadEvils(1);
            GameStatic.LoadMoon(2);
            GameStatic.LoadLevels(3);

            //bgMusic = Content.Load<Song>("Sound/music");
            //MediaPlayer.Play(bgMusic);
            //MediaPlayer.IsRepeating = true;
            TheGameScreen = new GameScreen();
            TheStartScreen = new StartScreen();
            TheStartScreen.BG = SpriteManager.ContentShell.Load<Texture2D>("Sprites/BG1");
            gameover=SpriteManager.ContentShell.Load<Texture2D>("Sprites/gameover");
            StartScreen.MainFont = new FontRenderer("Fonts/galacticfont", 3);
                     
            GameStatic.ThePlayer = AnimationSpecReader.PopulateAnimations<Player>(@".\Content\Specs\player.json");
            GameStatic.TheBasket = AnimationSpecReader.PopulateAnimations<Basket>(@".\Content\Specs\basket.json");

            GameStatic.timer = new GameTimer(this, 75);


            GameStatic.timer.Position = new Vector2(300, 200);
            Components.Add(GameStatic.timer);
        }
       

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
           
            // TODO: Unload any non ContentManager content here
        }
       
       
        
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
           
        bool bGameOver = false;

      
        bool bPaused = false;



     
          
        protected override void Update(GameTime gameTime)
        {
            SpriteManager.GameTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

      
            if (Keyboard.GetState().IsKeyDown(Keys.P))
            {
                bPaused = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.O))
            {
                bPaused = false;
            }

            if (bPaused)
                return;


            if (bGameOver)
                return;


            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                GameStatic.timer.Started = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                CurrentScreen = Screens.Game;
            }
            if(Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                GameStatic.SetLevel(0);
                GameStatic.timer.Started = true;
            }



            if (CurrentScreen == Screens.Start)
                TheStartScreen.Update();

            if (CurrentScreen == Screens.Game)
                TheGameScreen.Update();

            if (GameStatic.timer.Finished)
            {
                bGameOver = true;
                GameStatic.ThePlayer.ChangeAnimation("die", false);
                
            }

            /*
            for (int i = 0;i< TheTile.Count; i++) 
            {
                TheTile[i].Update();
            }*/
            /*
                for (int i = 0; i < TheEgg.Count; i++)
                {
                    TheEgg[i].Update();

                    if (ThePlayer.Bounds.Intersects(TheEgg[i].Bounds))
                    {

                        TheEgg[i].Position.X = ThePlayer.Position.X + 30;

                        TheEgg[i].Position.Y = ThePlayer.Position.Y - 5;

                        TheEgg[i].MyDirection = ThePlayer.CurrentDirection;

                        if (TheBasket.Bounds.Intersects(TheEgg[1].Bounds))
                        {

                            TheEgg[0].Position = new Vector2(900, 610);
                           
                        }
                        if (TheBasket.Bounds.Intersects(TheEgg[1].Bounds))
                        {

                            TheEgg[1].Position = new Vector2(900, 614);
                            
                        }
                        if (TheBasket.Bounds.Intersects(TheEgg[2].Bounds))
                        {

                            TheEgg[2].Position = new Vector2(900, 618);

                        }
                    }
                }*/

            /*
            if (ThePlayer.Bounds.Intersects(TheEvil.Bounds))
            {
                ThePlayer.ChangeAnimation("die");
                bGameOver = true;
            }
           */
           

          
             


                GameStatic.timer.Update(gameTime);

                base.Update(gameTime);
            }
      


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        Rectangle rBackground = new Rectangle(0,0,1000, 700);

        

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightSteelBlue);
            SpriteManager.spriteBatch.Begin();


            if (CurrentScreen == Screens.Start)
                TheStartScreen.Draw();

            if (CurrentScreen == Screens.Game)
                TheGameScreen.Draw();
            
                


            if (bGameOver)
            {
                SpriteManager.spriteBatch.Draw(gameover, new Rectangle(0, 0, 1000, 700), Color.White);
                StartScreen.MainFont.DrawTextWithWidth(new Vector2(350, 400), 1000, "GAME OVER)", 1, Color.Black);
               
                
            }
            
            GameStatic.timer.Draw(SpriteManager.spriteBatch);
            
           StartScreen.MainFont.DrawTextWithWidth(new Vector2(300, 5), 1000, "Level: " + GameStatic.iLevel, 1, Color.Black);
            
            SpriteManager.spriteBatch.End();

            // TODO: Add your drawing code here


               
                 

            base.Draw(gameTime);
        }









       
    }
}
