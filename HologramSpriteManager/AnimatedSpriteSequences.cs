﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
/*
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;
*/
using System.Threading.Tasks;
using System.IO;



namespace HologramSpriteManager
{
    public enum AnimationType
    {
        Character,
        Animated
    }

    public class CurrentFrame
    {
        public Rectangle SourceRectangle;
        public Texture2D SpriteMapTexture;
        public List<Collider> Colliders;

    }

    public class SpriteMap
    {
        public string SpriteMapName { get; set; }
        public Texture2D SpriteMapTexture { get; set; }
        public int Columns { get; set; }
        public int Rows { get; set; }
        private int width;
        private int height;

		public void PopulateTexture(bool fromExternal)
        {
			if (fromExternal) {
				using (var fileStream = File.Open (SpriteMapName,FileMode.Open,FileAccess.Read)) {
					//SpriteMapTexture = Texture2D.FromStream (SamsungKids.SamsungKids.graphics.GraphicsDevice, fileStream); 
				}
			}
			else
				SpriteMapTexture = SpriteManager.ContentShell.Load<Texture2D>(SpriteMapName);
				
            width = SpriteMapTexture.Width / Columns;
            height = SpriteMapTexture.Height / Rows;
        }
        public Rectangle GetRectangle(int iFrameRow,int iFrameColumn)
        {
            return new Rectangle(width * iFrameColumn, height * iFrameRow, width, height);
        }
    }

    public class AnimationFrame
    {
        public int weight { get; set; }
        public int map { get; set; }
        public int row { get; set; }
        public int column { get; set; }

        public List<Collider> colliders;

        public bool Compare(AnimationFrame frame)
        {
            bool bRet = false;
            if (weight == frame.weight && map == frame.map && row == frame.row && column == frame.column)
            {
                bRet = true;
            }
            return bRet;
        }
    }

    public class Collider
    {
        public string group { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int width { get; set; }
        public int height { get; set; }

    }

    public class Collision
    {
        public bool bCollided = false;
        public string LocalGroup = "";
        public string ExternalGroup = "";

    }

    public class AnimationSequence
    {
        public string AnimationName { get; set; }
        public float AnimationTime { get; set; }
        public int AnimationLoop { get; set; }
        public List<AnimationFrame> AnimationFrames { get; set; }
        //derived
        public int iTotalWeight=0;
        public float fLastAnimationStartTime;

        public AnimationSequence Clone()
        {
            AnimationSequence ret = new AnimationSequence();

            ret.AnimationName = AnimationName;
            ret.AnimationTime = AnimationTime;
            ret.AnimationLoop = AnimationLoop;
            ret.AnimationFrames = AnimationFrames;
            ret.iTotalWeight = iTotalWeight;


            return ret;
        }

        public void CalcWeight()
        {
            iTotalWeight = 0;
            for (int i=0;i<AnimationFrames.Count(); i++)
            {
                iTotalWeight += AnimationFrames[i].weight;
            }
        }

        public void SetTimeToFrame(AnimationFrame TargetFrame)
        {
            int iCurrentWeight = 0;
            for (int i = 0; i < AnimationFrames.Count(); i++)
            {
                iCurrentWeight += AnimationFrames[i].weight;
                bool bSameFrame = AnimationFrames[i].Compare(TargetFrame);
                if (bSameFrame)
                {

                    //what percentage of total time has passed.
                    float fPositionPercent = (iCurrentWeight / iTotalWeight);
                    //how much time is this:
                    float fTimePassed = AnimationTime * fPositionPercent;
                    fLastAnimationStartTime = SpriteManager.GameTime - fTimePassed;


                    break;
                }
            }       
        }

        public AnimationFrame GetCurrentFrameMeta()
        {

            float fPassedTime = SpriteManager.GameTime - fLastAnimationStartTime;
            if (fPassedTime > AnimationTime)
            {
                if (AnimationLoop == 0)
                {
                    fPassedTime = AnimationTime;
                }
                else
                {
                    fLastAnimationStartTime = SpriteManager.GameTime;
                }
            }

            int iFrame = 0;
            //if zero just use first frame
            if (fPassedTime > 0)
            {
                //weighting!
                //what percentage of total time has passed.
                float fPositionPercent = (fPassedTime / AnimationTime) * 100;
                //what is the value according to out weighting count
                float fTargetValue = (fPositionPercent / 100) * iTotalWeight;
                //loop through and find the frame
                int iCurrentWeight = 0;

                for (int i = 0; i < AnimationFrames.Count(); i++)
                {
                    iCurrentWeight += AnimationFrames[i].weight;
                    if (iCurrentWeight > fTargetValue)
                    {
                        iFrame = i;
                        break;
                    }
                }
            }
            return  AnimationFrames[iFrame];
        }
    }

    public class AnimatedSpriteSequences
    {
        public string Description { get; set; }
        public string sType { get; set; }
        public List<SpriteMap> SpriteMaps { get; set; }
        public List<AnimationSequence> AnimationSequences { get; set; }
        
        public AnimationType Type;

        public AnimatedSpriteSequences Clone()
        {
            AnimatedSpriteSequences ret = new AnimatedSpriteSequences();
            ret.Description =Description;
            ret.sType = sType;
            ret.SpriteMaps = SpriteMaps;
            ret.AnimationSequences = new List<AnimationSequence>();
            for (int i = 0; i < AnimationSequences.Count();i++ )
            {
                ret.AnimationSequences.Add( AnimationSequences[i].Clone());
            }
            ret.Type = Type;

            return ret;
        
        }

		public void PopulateSequence(bool fromExternal)
        {
            //load sprite maps:
            for (int i = 0; i < SpriteMaps.Count(); i++)
            {
				SpriteMaps[i].PopulateTexture(fromExternal);
            }
            //add weights
            for (int i = 0; i < AnimationSequences.Count(); i++)
            {
                AnimationSequences[i].CalcWeight();
            }

            //set enums
            switch(sType)
            {
                case "Character":
                    Type = AnimationType.Character;
                    break;
                case "Animated" :
                    Type = AnimationType.Animated;
                    break;
            }
            
        }

        string sCurrentSequence = "idle";
        public void SetAnimation(string sSequence)
        {
            sCurrentSequence = sSequence;
            for (int i = 0; i < AnimationSequences.Count(); i++)
            {
                if (AnimationSequences[i].AnimationName == sCurrentSequence)
                {
                    AnimationSequences[i].fLastAnimationStartTime = SpriteManager.GameTime;
                    break;
                }
            }
        }


        public void SetAnimation(string sSequence,bool bForceRestart)
        {
            if (!bForceRestart && sCurrentSequence == sSequence)
                return;

            SetAnimation(sSequence);
        }

        public void SetAnimationFlow(string sSequence,bool bForceRestart)
        {
            if (sCurrentSequence == sSequence)
            {
                if (!bForceRestart)
                {
                    return;
                }
                else
                {   
                    SetAnimation(sSequence);
                }
            }
            else
            {
                AnimationFrame _CurrentFrameMeta = new AnimationFrame();
                for (int i = 0; i < AnimationSequences.Count(); i++)
                {
                    if (AnimationSequences[i].AnimationName == sCurrentSequence)
                    {
                        //needs an algo to manipulate time
                        _CurrentFrameMeta = AnimationSequences[0].GetCurrentFrameMeta();
                        //AnimationSequences[i].fLastAnimationStartTime = SpriteManager.GameTime;
                        break;
                    }
                }
                sCurrentSequence = sSequence;
                for (int i = 0; i < AnimationSequences.Count(); i++)
                {
                    if (AnimationSequences[i].AnimationName == sCurrentSequence)
                    {
                        //needs an algo to manipulate time
                        AnimationSequences[i].SetTimeToFrame(_CurrentFrameMeta);
                        //AnimationSequences[i].fLastAnimationStartTime = SpriteManager.GameTime;
                        break;
                    }
                }
            }
        }

		public async void SetAnimation(string startAnim,string endAnim,int duration)
		{
			SetAnimation (startAnim);
			await Task.Delay(duration > 0 ? duration : (int)AnimationSequences.Find(a => a.AnimationName == startAnim).AnimationTime);
			SetAnimation (endAnim);
		}

        public CurrentFrame GetCurrentFrame()
        {
            //get sequence
            //zeroth one is standard (in case of spelling errors
            AnimationSequence CurrentSequence = AnimationSequences[0];
            for (int i = 0; i < AnimationSequences.Count(); i++)
            {
                if (AnimationSequences[i].AnimationName == sCurrentSequence)
                {
                    CurrentSequence = AnimationSequences[i];
                    break;
                }
            }
            AnimationFrame frameMeta =  CurrentSequence.GetCurrentFrameMeta();

            CurrentFrame frame = new CurrentFrame();
            frame.SpriteMapTexture = SpriteMaps[frameMeta.map].SpriteMapTexture;
            frame.SourceRectangle = SpriteMaps[frameMeta.map].GetRectangle(frameMeta.row,frameMeta.column);
            frame.Colliders = frameMeta.colliders;
            

            return frame;
        }

        /*
        /// <summary>
        /// Creates a new image from an existing image.
        /// </summary>
        /// <param name="bounds">Area to use as the new image.</param>
        /// <param name="source">Source image used for getting a part image.</param>
        /// <returns>Texture2D.</returns>
        Texture2D CreatePartImage(AnimationFrame afCurrentFrame)
        {
            //Declare variables
            Texture2D result;
            Color[]
                sourceColors,
                resultColors;


            Texture2D source = SpriteMaps[afCurrentFrame.map].SpriteMapTexture;
            Rectangle bounds = SpriteMaps[afCurrentFrame.map].GetRectangle(afCurrentFrame.row, afCurrentFrame.column);




            //Setup the result texture
            result = new Texture2D(SpriteManager._GraphicsDeviceManager.GraphicsDevice, bounds.Width, bounds.Height);
 
            //Setup the color arrays
            sourceColors = new Color[source.Height * source.Width];
            resultColors = new Color[bounds.Height * bounds.Width];
 
            //Get the source colors
            source.GetData<Color>(sourceColors);
 
            //Loop through colors on the y axis
            for (int y = bounds.Y; y < bounds.Height + bounds.Y; y++)
            {
                //Loop through colors on the x axis
                for (int x = bounds.X; x < bounds.Width + bounds.X; x++)
                {
                    //Get the current color
                    resultColors[x - bounds.X + (y - bounds.Y) * bounds.Width] =
                        sourceColors[x + y * source.Width];
                }
            }
 
            //Set the color data of the result image
            result.SetData<Color>(resultColors);
 
            //return the result
            return result;
        }*/
    }
}
