﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;



namespace HologramSpriteManager
{

    class AnimatedSprite
    {
        //setup
        public AnimatedSpriteSequences Sequences;

        public List<Collider> Colliders;

        public Vector2 Position;
        public bool Scrolling = false;

        public Vector2 Movement { get; set; }

        public AnimatedSprite Clone()
        {
            AnimatedSprite ret = new AnimatedSprite();
            ret.Sequences = Sequences.Clone();
            //var ctor = typeof(T).GetConstructor(new Type[] { typeof(AnimatedSpriteSequences) });
            //return (T)ctor.Invoke(new object[] { ret });
            return ret;
            
        }


        public AnimatedSprite(AnimatedSpriteSequences Meta)
        {
            Sequences = Meta;
            
        }
        public AnimatedSprite()
        {
        }

        public void ChangeAnimation(string sAnim)
        { 
            Sequences.SetAnimation(sAnim);

        }
        public void ChangeAnimation(string sAnim,bool bForceRestart)
        {
            Sequences.SetAnimation(sAnim,bForceRestart);

        }
        public void ChangeAnimation(string sAnim,bool bForceRestart,bool bFlow)
        {
            Sequences.SetAnimationFlow(sAnim,bForceRestart);

        }

		/// <summary>
		/// Changes the animation.
		/// </summary>
		/// <param name="startAnim">Start animation.</param>
		/// <param name="endAnim">End animation.</param>
		/// <param name="duration">Duration in miliseconds.Default value is 0. If not set, it'll use the duration from json.</param>
		public void ChangeAnimation(string startAnim,string endAnim,int duration = 0)
		{
			Sequences.SetAnimation(startAnim,endAnim,duration);
		}

        SpriteEffects ActiveEffect = SpriteEffects.None;
        public void SetEffect(SpriteEffects Effect)
        {
            ActiveEffect = Effect;
        }


        public bool CheckIntersect(AnimatedSprite External)
        { 
            Rectangle MyBounds = Bounds;
            Rectangle ExternalBounds = External.Bounds;
            if (MyBounds.Intersects (ExternalBounds) || MyBounds.Contains(ExternalBounds) || ExternalBounds.Contains(MyBounds))
            {
                return true;
            }

            return false;
        }

        public Collision CheckCollision(AnimatedSprite External)
        {
            Collision ret = new Collision();

            if(Colliders == null || External.Colliders==null)
                return ret;

            //loop every local collider
            for (int i = 0; i < Colliders.Count;i++ )
            {
                Rectangle rCurrentLocal = new Rectangle((int)Position.X +  Colliders[i].x,(int)Position.Y + Colliders[i].y,Colliders[i].width,Colliders[i].height);
                //loop every external collider
                for (int j = 0; j < External.Colliders.Count;j++ )
                {
                    Rectangle rCurrentExternal = new Rectangle((int)External.Position.X + External.Colliders[j].x, (int)External.Position.Y + External.Colliders[j].y, External.Colliders[j].width, External.Colliders[j].height);
                    if(rCurrentLocal.Intersects(rCurrentExternal))
                    {
                        ret.bCollided = true;
                        ret.LocalGroup = Colliders[i].group;
                        ret.ExternalGroup = External.Colliders[j].group;
                        return ret;
                    }
                }

            }

            return ret;
        }
        public List<Collision> CheckAllCollisions(AnimatedSprite External)
        {
            List<Collision> ret = new List<Collision>();

            if (Colliders == null || External.Colliders == null)
                return ret;

            //loop every local collider
            for (int i = 0; i < Colliders.Count; i++)
            {
                Rectangle rCurrentLocal = new Rectangle((int)Position.X + Colliders[i].x, (int)Position.Y + Colliders[i].y, Colliders[i].width, Colliders[i].height);
                //loop every external collider
                for (int j = 0; j < External.Colliders.Count; j++)
                {
                    Rectangle rCurrentExternal = new Rectangle((int)External.Position.X + External.Colliders[j].x, (int)External.Position.Y + External.Colliders[j].y, External.Colliders[j].width, External.Colliders[j].height);
                    if (rCurrentLocal.Intersects(rCurrentExternal))
                    {
                        Collision Current = new Collision();
                        Current.bCollided = true;
                        Current.LocalGroup = Colliders[i].group;
                        Current.ExternalGroup = External.Colliders[j].group;
                        ret.Add(Current);
                    }
                }

            }

            return ret;
        }

        public void Draw()
        {


            CurrentFrame frame = Sequences.GetCurrentFrame();
            Colliders = frame.Colliders;
            //Console.WriteLine(frame.SourceRectangle);
            int X = (int)Position.X;
            int Y = (int)Position.Y;
            if (Scrolling)
            {
                X += SpriteManager.iXOffset;
                Y += SpriteManager.iYOffset;
            }
            Rectangle destinationRectangle = new Rectangle(X,Y, frame.SourceRectangle.Width, frame.SourceRectangle.Height);
           
            SpriteManager.spriteBatch.Draw(frame.SpriteMapTexture, destinationRectangle, frame.SourceRectangle, Color.White, 0f, new Vector2(), ActiveEffect, 0);
         
        }

		public void DrawWithAlpha(Color color)
		{
			CurrentFrame frame = Sequences.GetCurrentFrame();
			//Console.WriteLine(frame.SourceRectangle);
            int X = (int)Position.X;
            int Y = (int)Position.Y;
            if (Scrolling)
            {
                X += SpriteManager.iXOffset;
                Y += SpriteManager.iYOffset;
            }
			Rectangle destinationRectangle = new Rectangle(X, Y, frame.SourceRectangle.Width, frame.SourceRectangle.Height);
			SpriteManager.spriteBatch.Draw(frame.SpriteMapTexture, destinationRectangle, frame.SourceRectangle, color);

		}

        public Rectangle Bounds
        {
            get
            {
                CurrentFrame current = Sequences.GetCurrentFrame();
                return new Rectangle((int)Position.X, (int)Position.Y, current.SourceRectangle.Width, current.SourceRectangle.Height);
                         // width, height);
            }
        }

    }
}
